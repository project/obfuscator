<?php

namespace Drupal\obfuscator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures obfuscator settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'obfuscator_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'obfuscator.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $obfuscator_config = $this->config('obfuscator.settings');

    $form['obfuscator_core'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Library core'),
      '#description' => $this->t('Obfuscate the core version from the core js'),
      '#default_value' => $obfuscator_config->get('obfuscator_core'),
    ];

    $form['obfuscator_modules'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Core modules'),
      '#description' => $this->t('Obfuscate the core version from the module core js'),
      '#default_value' => $obfuscator_config->get('obfuscator_modules'),
    ];

    $form['obfuscator_contrib'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Contrib modules'),
      '#description' => $this->t('Obfuscate the core version from the contrib modules js'),
      '#default_value' => $obfuscator_config->get('obfuscator_contrib'),
    ];

    $form['obfuscator_custom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Custom modules'),
      '#description' => $this->t('Obfuscate the core version from the custom modules js'),
      '#default_value' => $obfuscator_config->get('obfuscator_custom'),
    ];
  
    $form['obfuscator_core_themes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Core themes'),
      '#description' => $this->t('Obfuscate the core version from the core themes js'),
      '#default_value' => $obfuscator_config->get('obfuscator_core_themes'),
    ];
  
    $form['obfuscator_core_assets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Core assets'),
      '#description' => $this->t('Obfuscate the core version from the core assets js'),
      '#default_value' => $obfuscator_config->get('obfuscator_core_assets'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('obfuscator.settings')
      ->set('obfuscator_core', $values['obfuscator_core'])
      ->set('obfuscator_modules', $values['obfuscator_modules'])
      ->set('obfuscator_contrib', $values['obfuscator_contrib'])
      ->set('obfuscator_custom', $values['obfuscator_custom'])
      ->set('obfuscator_core_themes', $values['obfuscator_core_themes'])
      ->set('obfuscator_core_assets', $values['obfuscator_core_themes'])
      ->save();

    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
